package com.gl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.gl.model.IDCard;  
import com.gl.model.Person;  
import com.gl.repository.PersonRepository;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.boot.CommandLineRunner;  
import java.util.ArrayList;  
import java.util.List;



@SpringBootApplication
public class GloombApplication implements CommandLineRunner {
	
	@Autowired
    private PersonRepository personRepository;

	public static void main(String[] args) {
		SpringApplication.run(GloombApplication.class, args);
	}
	@Override
    public void run(String... strings) throws Exception {
        // save idCard along with persons
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Anurag Sharma", new IDCard()));
        persons.add(new Person("RajuRam", new IDCard()));
        persons.add(new Person("ShyamLal", new IDCard()));
        personRepository.saveAll(persons);
    }
}
